npm i установить зависимости

запуск командной строки PostgreSQL:
sudo -u postgres psql

Cоздать бд:
CREATE DATABASE НАЗВАНИЕ БАЗЫ OWNER ИМЯ ПОЛЬЗОВАТЕЛЯ;

Накатить миграции:
npx sequelize-cli db:migrate:all

Накатить сиды:
npx sequelize-cli db:seed:all

в папке server создать файл .env и задать название базы, имя пользователя, пароль:
DB_NAME=test
DB_USER=test
DB_PASS=test
SERVER_PORT=3001

Запуск приложения

npm start в папке server поднимает локальный dev-сервер на http://localhost:3001/
npm start в папке client поднимает приложение на http://localhost:3000/
