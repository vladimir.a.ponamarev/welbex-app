const express = require('express');
const { Op } = require('sequelize');
const { TableData } = require('../db/models');

const tableDataRouter = express.Router();

tableDataRouter
  .route('/alldata')
  .get(async (req, res) => {
    try {
      const allData = await TableData.findAll();
      res.json(allData);
    } catch (error) {
      console.log(error);
      res.sendStatus(500);
    }
  })

  .post(async (req, res) => {
    try {
      const newData = await TableData.create({ ...req.body, userId: 4 });
      res.json(newData);
    } catch (error) {
      console.log(error);
      res.sendStatus(500);
    }
  });
tableDataRouter.post('/wordsall', async (req, res) => {
  try {
    const { selectedFilterByColimn, selectedFilterByParams, searchQuery } =
      req.body;
    let result;
    if (
      searchQuery.trim() === '' ||
      selectedFilterByColimn.trim() === '' ||
      selectedFilterByParams.trim() === ''
    ) {
      result = await TableData.findAll();
      res.json(result);
    } else if (selectedFilterByParams === 'contain') {
      const result = await TableData.findAll({
        where: {
          [selectedFilterByColimn]: {
            [Op.like]: `%${searchQuery}%`
          }
        }
      });
      res.json(result);
    } else if (selectedFilterByParams === 'equals') {
      const result = await TableData.findAll({
        where: {
          [selectedFilterByColimn]: {
            [Op.eq]: searchQuery
          }
        }
      });
      res.json(result);
    } else if (selectedFilterByParams === 'more') {
      {
        const result = await TableData.findAll({
          where: {
            [selectedFilterByColimn]: {
              [Op.gt]: searchQuery
            }
          }
        });
        res.json(result);
      }
    } else {
      {
        const result = await TableData.findAll({
          where: {
            [selectedFilterByColimn]: {
              [Op.lt]: searchQuery
            }
          }
        });
        res.json(result);
      }
    }
  } catch (error) {
    console.log(error);
    res.sendStatus(500);
  }
});

module.exports = tableDataRouter;
