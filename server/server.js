const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const tableDataRouter = require('./router/tableRouter');
require('dotenv').config();

const app = express();
const PORT = process.env.PORT || 3001;

app.use(cors());
app.use(morgan('dev'));
app.use(express.json());

// app.use((req, res, next) => {
//   res.header('Access-Control-Allow-Origin', '*');
//   res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//   next();
// });

app.use('/api/', tableDataRouter);

app.listen(PORT, () => console.log(`Server has started on PORT ${PORT}`));
