'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TableData extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    // eslint-disable-next-line no-unused-vars
    static associate(models) {
      // define association here
    }
  }
  TableData.init(
    {
      date: DataTypes.DATE,
      name: DataTypes.STRING,
      quantity: DataTypes.INTEGER,
      distance: DataTypes.INTEGER
    },
    {
      sequelize,
      modelName: 'TableData'
    }
  );
  return TableData;
};
