/** @type {import('sequelize-cli').Migration} */
const fetch = require('node-fetch-commonjs');
module.exports = {
  async up(queryInterface, Sequelize) {
    const res = await fetch(
      'http://www.filltext.com/?rows=200&id={number|200}&firstName={firstName}&delay=3&date={date|1-1-1900,1-6-2023}&numberRange={numberRange|100,1000}&number={number|5000}'
    );
    const allData = await res.json();
    await queryInterface.bulkInsert(
      'TableData',
      allData.map(data => ({
        date: data.date,
        name: data.firstName,
        quantity: data.numberRange,
        distance: data.number,
        createdAt: new Date(),
        updatedAt: new Date()
      })),
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
