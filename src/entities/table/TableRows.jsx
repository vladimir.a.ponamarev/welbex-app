import React from 'react';
import { Cell, CellWrapper } from './ui';

const TableRows = ({ dataToRender }) => {
  return (
    <>
      {dataToRender?.map(el => (
        <CellWrapper key={el.id}>
          <Cell>
            <p>{new Date(el.date).toLocaleDateString()}</p>
          </Cell>
          <Cell>
            <p>{el.name}</p>
          </Cell>
          <Cell>
            <p>{el.quantity}</p>
          </Cell>
          <Cell>
            <p>{el.distance}</p>
          </Cell>
        </CellWrapper>
      ))}
    </>
  );
};

export default TableRows;
