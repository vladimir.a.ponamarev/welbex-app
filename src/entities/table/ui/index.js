import styled from 'styled-components';

export const Cell = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  border-width: 0.2px;
  border-style: solid;
  border-color: #c7c7c7;
  height: 42px;
  padding-left: 10px;
`;

export const CellWrapper = styled.div`
  display: grid;
  grid-template-columns: 1.5fr 1fr 1.6fr 2.5fr;
  justify-content: center;
  align-items: center;
  height: 42px;
`;
