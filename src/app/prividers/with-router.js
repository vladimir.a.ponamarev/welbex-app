import { Suspense } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Spinner } from '../../shares/ui/spinner';

export const withRouter = component => () => {
  return (
    <BrowserRouter>
      <Suspense fallback={<Spinner />}>{component()}</Suspense>
    </BrowserRouter>
  );
};
