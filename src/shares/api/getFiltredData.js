import axios from 'axios';

export const GetFiltredData = (req, setData) => {
  const apiUrl = `http://localhost:3001/api/wordsall`;
  try {
    axios.post(apiUrl, req).then(res => {
      setData(res.data);
    });
  } catch (error) {
    console.log(error);
  }
};
