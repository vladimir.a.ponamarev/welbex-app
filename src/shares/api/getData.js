import axios from 'axios';

export function getData() {
  return new Promise((resolve, reject) => {
    const apiUrl = `http://localhost:3001/api/alldata`;

    setTimeout(() => {
      axios
        .get(apiUrl)
        .then(response => {
          resolve(response.data);
        })
        .catch(error => {
          reject(error);
        });
    }, 1000);
  });
}
