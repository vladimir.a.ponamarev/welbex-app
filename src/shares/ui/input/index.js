import styled from 'styled-components';

export const Input = styled.input`
  border-radius: 5px;
  border: 0.5px solid slategray;
  padding: 5px;
  text-align: start;
  @media (max-width: 426px) {
    width: 90vw;
  }
`;
