import styled from 'styled-components';

export const SelectContainer = styled.div`
  display: flex;
`;

export const Select = styled.select`
  border-radius: 5px;
  border: 0.5px solid slategray;
  padding: 3px;
  text-align: start;
`;

export const Button = styled.button`
  border-radius: 5px;
  border: 0.5px solid slategray;
  padding: 2px;
  margin-left: 1px;
`;
