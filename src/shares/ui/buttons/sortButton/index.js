import styled from 'styled-components';

export const SortButton = styled.button`
  border: none;
  background: transparent;
  padding: 0;
  margin: 0 5px;
  cursor: pointer;
`;
