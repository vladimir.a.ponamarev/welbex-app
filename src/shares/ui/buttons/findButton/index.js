import styled from 'styled-components';

export const FindButton = styled.button`
  border-radius: 5px;
  border: 0.5px solid slategray;
  padding: 5px;
  text-align: center;
  cursor: pointer;
  width: 80px;
`;
