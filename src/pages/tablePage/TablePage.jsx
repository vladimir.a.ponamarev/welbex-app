import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { getData } from '../../shares/api/getData';
import SortSection from '../../features/commonElement/ui/sortSection/SortSection';
import { MainContainer } from '../../features/commonElement/commonElementStyles';
import MySelect from '../../features/commonElement/ui/select/Select';
import MyInput from '../../features/commonElement/ui/input/Input';
import { filterOptions } from '../../features/commonElement/ui/select/filterOptions';
import { sortOptions } from '../../features/commonElement/ui/select/sortOptions';
import { Spinner, SpinnerContainer } from '../../shares/ui/spinner';
import { FindButton } from '../../shares/ui/buttons/findButton';
import { GetFiltredData } from '../../shares/api/getFiltredData';
import Pagination from '../../features/pogination/Pagination';
import TableRows from '../../entities/table/TableRows';

export default function TablePage() {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [selectedFilterByColimn, setSelectedFilterByColumn] = useState('');
  const [selectedFilterByParams, setSelectedFilterByParams] = useState('');
  const [filtredData, setFiltredData] = useState('');
  const [selectedSort, setSelectedSort] = useState('');
  const [searchQuery, setSearchQuery] = useState('');
  const [sortDirection, setSortDirection] = useState('asc');
  const [correctValues, setCorrectValues] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);

  const handlePageClick = pageNumber => {
    setCurrentPage(pageNumber);
  };

  const [dataToRender, setDataToRender] = useState([]);

  const fetchTableData = useCallback(() => {
    setIsLoading(true);
    getData()
      .then(responseData => {
        setData(responseData);
      })
      .catch(error => {
        console.log(error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  useEffect(() => {
    fetchTableData(setData, setIsLoading);
  }, [fetchTableData]);

  const toggleSortDirection = useCallback(string => {
    setSortDirection(prevDirection =>
      prevDirection === 'asc' ? 'desc' : 'asc'
    );
    setSelectedSort(string);
  }, []);

  const submitHandler = () => {
    if (
      (selectedFilterByColimn === 'name' && typeof searchQuery !== 'string') ||
      ((selectedFilterByColimn === 'quantity' ||
        selectedFilterByColimn === 'distance') &&
        isNaN(searchQuery)) ||
      ((selectedFilterByColimn === 'quantity' ||
        selectedFilterByColimn === 'distance') &&
        selectedFilterByParams === 'contain')
    ) {
      setCorrectValues(true);
      return null;
    }
    GetFiltredData(
      { selectedFilterByColimn, selectedFilterByParams, searchQuery },
      setFiltredData
    );
    setCorrectValues(false);
    setCurrentPage(1);
  };

  const sortedData = useMemo(() => {
    if (selectedSort && filtredData.length) {
      return [...filtredData].sort((elementA, elementB) => {
        const a = elementA[selectedSort];
        const b = elementB[selectedSort];

        if (selectedSort === 'date' || selectedSort === 'name') {
          return sortDirection === 'asc'
            ? (a || '').localeCompare(b || '')
            : (b || '').localeCompare(a || '');
        } else {
          return sortDirection === 'asc' ? a - b : b - a;
        }
      });
    } else if (selectedSort) {
      return [...data].sort((elementA, elementB) => {
        const a = elementA[selectedSort];
        const b = elementB[selectedSort];

        if (selectedSort === 'date' || selectedSort === 'name') {
          return sortDirection === 'asc'
            ? (a || '').localeCompare(b || '')
            : (b || '').localeCompare(a || '');
        } else {
          return sortDirection === 'asc' ? a - b : b - a;
        }
      });
    } else if (filtredData.length) {
      return [...filtredData].sort((elementA, elementB) => {
        const a = elementA[selectedSort];
        const b = elementB[selectedSort];

        if (selectedSort === 'date' || selectedSort === 'name') {
          return sortDirection === 'asc'
            ? (a || '').localeCompare(b || '')
            : (b || '').localeCompare(a || '');
        } else {
          return sortDirection === 'asc' ? a - b : b - a;
        }
      });
    } else return data;
  }, [selectedSort, data, sortDirection, filtredData]);

  const totalPages = sortedData.length;

  useEffect(() => {
    let firstIndex;
    let lastIndex;
    if (sortedData.length > 20) {
      firstIndex = 20 * currentPage - 20;
      lastIndex = firstIndex + 20;
      return setDataToRender(sortedData.slice(firstIndex, lastIndex));
    } else {
      firstIndex = 0;
      lastIndex = totalPages;
      return setDataToRender(sortedData.slice(firstIndex, lastIndex));
    }
  }, [sortedData, currentPage]);

  const filterByColumn = filter => {
    setSelectedFilterByColumn(filter);
  };

  const filterByParams = value => {
    setSelectedFilterByParams(value);
  };

  if (isLoading) {
    return (
      <MainContainer>
        <SpinnerContainer>
          <Spinner />
        </SpinnerContainer>
      </MainContainer>
    );
  }

  return (
    <>
      <MainContainer>
        <MySelect
          defaultValue="Choose column"
          value={selectedFilterByColimn}
          onChange={filterByColumn}
          options={sortOptions}
        />
        <MySelect
          defaultValue="Params"
          value={selectedFilterByParams}
          onChange={filterByParams}
          options={filterOptions}
        />
        <MyInput value={searchQuery} onChange={setSearchQuery} />
        <FindButton type="button" onClick={submitHandler}>
          Find
        </FindButton>
      </MainContainer>
      <Pagination
        currentPage={currentPage}
        handlePageClick={handlePageClick}
        totalPages={totalPages}
      />
      <MainContainer>
        {' '}
        {correctValues ? <h5>Enter correct values</h5> : null}
      </MainContainer>
      <SortSection toggleSortDirection={toggleSortDirection} />
      <TableRows dataToRender={dataToRender} />
    </>
  );
}
