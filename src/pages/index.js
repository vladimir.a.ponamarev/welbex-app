import { lazy } from 'react';
import { Route, Routes } from 'react-router-dom';
const TablePage = lazy(() => import('./tablePage/TablePage'));

export const Routing = () => {
  return (
    <Routes>
      <Route exact path="/" element={<TablePage />} />
    </Routes>
  );
};
