import './App.css';
import { withProviders } from './app/prividers';
import { Routing } from './pages';

const App = () => {
  return (
    <div className="App">
      <Routing />
    </div>
  );
};

export default withProviders(App);
