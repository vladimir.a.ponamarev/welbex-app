import styled from 'styled-components';

export const MainContainer = styled.div`
  display: flex;
  justify-content: center;
  margin: 20px;
`;

export const ElementContainer = styled.div`
  width: 100vw;
  height: 10vh;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  @media (max-width: 705px) {
    flex-direction: column;
    height: 20vh;
  }
`;

export const Input = styled.input`
  border-radius: 5px;
  border: 0.5px solid slategray;
  padding: 5px;
  text-align: start;
  @media (max-width: 426px) {
    width: 90vw;
  }
`;

export const SelectContainer = styled.div`
  display: flex;
`;

export const Select = styled.select`
  border-radius: 5px;
  border: 0.5px solid slategray;
  padding: 3px;
  text-align: start;
`;

export const Button = styled.button`
  border-radius: 5px;
  border: 0.5px solid slategray;
  padding: 2px;
  margin-left: 1px;
`;
