import React, { useState } from 'react';
import Frame from './Frame.png';
import { Cell, CellWrapper } from '../../../../entities/table/ui';
import { SortButton } from '../../../../shares/ui/buttons/sortButton/index';

export default function SortSection({ toggleSortDirection }) {
  return (
    <CellWrapper>
      <Cell>
        <p>Date</p>
        <SortButton
          type="button"
          value="date"
          onClick={() => toggleSortDirection('date')}
        >
          <img src={Frame} alt="Sort by date" />
        </SortButton>
      </Cell>
      <Cell>
        <p>Name</p>
        <SortButton
          type="button"
          value="name"
          onClick={() => toggleSortDirection('name')}
        >
          <img src={Frame} alt="Sort by name" />
        </SortButton>
      </Cell>
      <Cell>
        <p>Quantity</p>
        <SortButton
          type="button"
          value="quantity"
          onClick={() => toggleSortDirection('quantity')}
        >
          <img src={Frame} alt="Sort by quantity" />
        </SortButton>
      </Cell>
      <Cell>
        <p>Distance</p>
        <SortButton
          type="button"
          value="distance"
          onClick={() => toggleSortDirection('distance')}
        >
          <img src={Frame} alt="Sort by distance" />
        </SortButton>
      </Cell>
    </CellWrapper>
  );
}
