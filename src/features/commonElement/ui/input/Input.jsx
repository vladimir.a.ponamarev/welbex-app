import React from 'react';
import { Input } from '../../../../shares/ui/input';

function MyInput({ value, onChange }) {
  return (
    <Input
      value={value}
      onChange={e => onChange(e.target.value)}
      placeholder="find a data"
    />
  );
}

export default MyInput;
