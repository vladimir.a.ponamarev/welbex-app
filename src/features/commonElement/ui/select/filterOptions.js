export const filterOptions = [
  { value: 'equals', name: '=' },
  { value: 'contain', name: 'contain' },
  { value: 'more', name: '>' },
  { value: 'less', name: '<' }
];
