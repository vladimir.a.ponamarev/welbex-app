export const sortOptions = [
  { value: 'name', name: 'By name' },
  { value: 'quantity', name: 'By quantity' },
  { value: 'distance', name: 'By distance' }
];
