import React from 'react';
import { Select } from '../../../../shares/ui/select';

function MySelect({ options, defaultValue, value, onChange }) {
  return (
    <Select value={value} onChange={e => onChange(e.target.value)}>
      <option disabled value="">
        {defaultValue}
      </option>
      {options.map(option => (
        <option key={option.value} value={option.value}>
          {option.name}
        </option>
      ))}
    </Select>
  );
}

export default MySelect;
