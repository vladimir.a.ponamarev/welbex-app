import React from 'react';
import './page.css';

function Pagination({ currentPage, handlePageClick, totalPages }) {
  const pageSize = 20;
  const pagesCount = Math.ceil(totalPages / pageSize);
  const pages = [];
  for (let i = 1; i <= pagesCount; i++) {
    pages.push(i);
  }

  const renderPageNumbers = () => {
    const visiblePages = [];
    let lastVisiblePage = 0;

    for (let i = 0; i < pages.length; i++) {
      if (Math.abs(pages[i] - currentPage) <= 2) {
        visiblePages.push(pages[i]);
        lastVisiblePage = pages[i];
      } else {
        if (lastVisiblePage !== 0 && pages[i] === lastVisiblePage + 1) {
          visiblePages.push('...');
        }
        lastVisiblePage = 0;
      }
    }

    return visiblePages.map(page => {
      if (page === '...') {
        return (
          <span key={page} className="paginationEllipsis">
            ...
          </span>
        );
      }
      return (
        <button
          type="button"
          key={page}
          className={`paginationButton${page === currentPage ? 'Active' : ''}${
            page !== currentPage && 'Disabled'
          }`}
          onClick={() => handlePageClick(page)}
        >
          {page}
        </button>
      );
    });
  };

  return (
    <div className="pagination">
      <button
        type="button"
        className="paginationButton"
        onClick={() => handlePageClick(currentPage - 3)}
        disabled={currentPage === 1}
      >
        {'<'}
      </button>

      {renderPageNumbers()}

      <button
        type="button"
        className="paginationButton"
        onClick={() => handlePageClick(currentPage + 3)}
        disabled={currentPage === pagesCount}
      >
        {'>'}
      </button>
    </div>
  );
}

export default Pagination;
